﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AnimationImage : MonoBehaviour { 
    public Sprite idleSprite;
    public Sprite noizeSprite;
    public Sprite bignoizeSprite;
    public Sprite glitchSprite;
    public Sprite terribleSprite;
    public Sprite Happy;
    

    public enum State
    {
        idle,
        noize,
        bignoize,
        glitch,
        terrible,
        EOF
    }

    public State state ;

    float clocktime = 0;
    bool flag = true;
    bool imagecrack = false;




    // Use this for initialization
    void Start () {
        state = State.idle;
        clocktime = Time.time;        
	}
	
	// Update is called once per frame
	void Update () {

        if ((int)((Time.time - clocktime) * 20) % 2 != 0)
        { 
            flag = true;
            return;
        }
            if (flag)
                flag = false;
            else
            return;
            switch (state)
        {
            case State.idle:
                if(Random.Range(0, 5) < 3)
                { 
                    changeSprite(noizeSprite);
                }
                else
                {
                    changeSprite(idleSprite);
                }
                break;
            case State.bignoize:
                if (Random.Range(0,5) <3)
                {
                    changeSprite(bignoizeSprite);
                }
                else
                {
                    changeSprite(noizeSprite);
                }
                break;
            case State.noize:
                if (Random.Range(0, 3) < 2)
                {
                    changeSprite(idleSprite);
                }
                else if (Random.Range(0,2) < 1)
                {
                    changeSprite(noizeSprite);
                }
                else
                {
                    changeSprite(bignoizeSprite);
                }
                break;
            case State.glitch:
                if (Random.Range(0, 5) < 2)
                {
                    changeSprite(noizeSprite);
                } 
                else if(Random.Range(0,5) < 2)
                {
                    changeSprite(bignoizeSprite);
                }
                else
                {
                    changeSprite(glitchSprite);
                }
                break;
            case State.terrible:
                if (Random.Range(0, 5) < 1)
                {
                    changeSprite(noizeSprite);
                    imagecrack = true;
                }
                else if (Random.Range(0, 5) < 1)
                {
                    changeSprite(bignoizeSprite);
                    imagecrack = true;
                }
                else if (Random.Range(0,5) < 3)
                {
                    if (imagecrack)
                    {
                        changeSprite(terribleSprite);
                        imagecrack = false;
                    }

                }
                else
                {
                    if (imagecrack)
                    {
                        changeSprite(glitchSprite);
                        imagecrack = false;
                    }
                }
                break;
            default:
                changeSprite(Happy);
                break;
        }
	}

    void changeSprite(Sprite sp)
    {
        if (this.gameObject.GetComponent<Image>().sprite != sp) {
            this.gameObject.GetComponent<Image>().sprite = sp;
        }
    }


    /// <summary>
    /// 0 : idle, 1 : noize, 2 : glitch, 3: terrible >4: terrible
    /// </summary>
    /// <param name="i"></param>
    public void SetAnchor(int i)
    {
        switch(i)
        {
            case 0:
                state = State.idle;
                break;
            case 1:
                state = State.noize;
                break;
            case 2:
                state = State.bignoize;
                break;
            case 3:
                state = State.glitch;
                break;
            case 4:
                state = State.terrible;
                break;
            default:
                state = State.EOF;
                break;
        }
    }

    
}
