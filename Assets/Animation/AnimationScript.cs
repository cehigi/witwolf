﻿using UnityEngine;
using System.Collections;

public class AnimationScript : MonoBehaviour {

    private static int clocktime = 0;
    private static int maxclock = 60;

    public Sprite idleSprite;
    public Sprite noizeSprite;
    public Sprite glitchSprite;
    public Sprite terribleSprite;
    public Sprite bignoizeSprite;

    public enum State
    {
        idle,
        noize,
        bignoize,
        glitch,
        terrible,
        EOF
    }

    public State state ;



    // Use this for initialization
    void Start () {
        state = State.idle;
	}
	
	// Update is called once per frame
	void Update () {

        Clock();

        switch(state)
        {
            case State.idle:
                if (clocktime > maxclock / 2 &&clocktime < maxclock / 2 + maxclock/5)
                {
                    changeSprite(noizeSprite);
                }
                else
                {
                    changeSprite(idleSprite);
                }
                break;
            case State.noize:
                if ((clocktime > maxclock / 10 && clocktime < maxclock / 10 * 2) ||
                    (clocktime > maxclock / 10 * 3 && clocktime < maxclock / 10 * 4))
                {
                    changeSprite(bignoizeSprite);
                }
                else
                {
                    changeSprite(noizeSprite);
                }
                break;
        }
	}

    void changeSprite(Sprite sp)
    {
        if (this.gameObject.GetComponent<SpriteRenderer>().sprite != sp) {
            Debug.Log("hey");
            this.gameObject.GetComponent<SpriteRenderer>().sprite = sp;
        }
    }

    public void SetAnchor(int i)
    {
        switch(i)
        {
            case 0:
                state = State.idle;
                break;
            case 1:
                state = State.noize;
                break;
            case 2:
                state = State.glitch;
                break;
            case 3:
            default:
                state = State.terrible;
                break;
        }
    }

    void Clock()
    {
        clocktime++;
        if (clocktime > maxclock)
            clocktime = 0;
    }

    public int Clocktime
    {
        get
        {
            return clocktime;
        }
    }
}
