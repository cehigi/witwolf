﻿using UnityEngine;
using System.Collections;

public class SpringRect : MonoBehaviour {
    Rect _rect;
    Rect _src_rect;

    public Rect DstRect
    {
        get
        {
            if (_rect == null)
            {
                _rect = new Rect(_src_rect);
            }
            //if (_rect == null)
            //{
            //    Debug.LogAssertion("SpringRect: Rect component not found.");
            //}
            return _rect;
        }
        set
        {
            _rect = value;
        }
    }

    void Init()
    {
        _src_rect = GetComponent<RectTransform>().rect;
    }

    // TODO: when this commponent enable
    // Init()


    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        _src_rect.Set(DstRect.x, DstRect.y, DstRect.width, DstRect.height);
    }
}
