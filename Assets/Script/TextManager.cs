﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TextManager : MonoBehaviour {

    static TextManager _t;
    public static TextManager textManager
    {
        get
        {
            if (_t == null)
            {
                _t = FindObjectOfType<TextManager>();
                DontDestroyOnLoad(_t);
            }
            return _t;
        }
    }

    [SerializeField]
    private TextShowingScript TextBox;
    [SerializeField]
    private TextShowingScript TVBox;

    List<string> strArr;

    string wolfText;
    string[] normalText;
    public string anchorText;

    // Use this for initialization
    void Start() {
        strArr = new List<string>();
        normalText = new string[4];
        LoadDialogue();
        ShowDialogue();
    }
    int count = 0;
    // Update is called once per frame
    void Update() {
        if (count > 10)
            count = 10;
        count++;

        if (count == 9)
            ShowDialogue();
    }

    /// <summary>
    /// get All string of Suspects
    /// </summary>
    public void LoadDialogue()
    {
        strArr.Clear();
        // Do something

    }

    public void ReloadDialog(EventData data)
    {
        List<int> a = new List<int>();
        int j;

        wolfText = data.WolfText;
        anchorText = data.AnchorText;
        for (int i = 0; i < 4; i++)
        {
            do
            {
                j = Random.Range(0, 4);
            } while (a.Contains(j));

            a.Add(j);

            switch(i)
            {
                case 0:
                    normalText[j] = data.NormalText_1;
                    break;
                case 1:
                    normalText[j] = data.NormalText_2;
                    break;
                case 2:
                    normalText[j] = data.NormalText_3;
                    break;
                case 3:
                    normalText[j] = data.NormalText_4;
                    break;
            }
        }
    }
    /// <summary>
    /// show Dialogue on index (index less than 4)
    /// </summary>
    /// <param name="index"></param>
    public void ShowNormalDialogue(int index)
    {
        index = Mathf.Clamp(index, 0, 3);

        //string script = "Normal Normal!";
        string script = normalText[index];

        TextBox.setString(script);
        TextBox.Show();
    }


    /// <summary>
    /// Show Dialogue of Wolf
    /// </summary>
    public void ShowWolfDialogue()
    {
        //string script = "Woof Woof!";
        string script = wolfText;

        TextBox.setString(script);
        TextBox.Show();
    }

    /// <summary>
    /// Show God father's Dialogue
    /// </summary>
    public void ShowTVDialogue()
    {
        //string script = "TV TV!";
        string script = anchorText;
        
        TVBox.setString(script);
        TVBox.Show();
    }

    void ShowDialogue()
    {
        //TextBox.setString(strArr[0]);
        TextBox.Show();
        TextBox.ShowFull();
    }

    public void ClearSelectionDialogue()
    {
        TextBox.setString("");
        TextBox.Show();
    }

    public void ClearTVDialogue()
    {
        TVBox.setString("");
        TVBox.Show();
    }


}
