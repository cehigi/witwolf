﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class GameManager : MonoBehaviour {

    private static GameManager _g;
    public static GameManager gameManager
    {
        get
        {
            if (_g == null)
            {
                _g = FindObjectOfType<GameManager>();
                DontDestroyOnLoad(_g);
            }
            return _g;
        }
    }



    static SceneController sc;
    static List<EventData> EventPool = new List<EventData>();

    /*-----Static Game Setting-----*/
    static int MAX_WOLF_SUSPECTS_NUM = 5;

    /*-----GameStatus-----*/
    public int wolf_index = -1;

    public int Distopia = 0;

    private int[] asdf = new int [10];

    [SerializeField]
    AnimationImage[] Man;


    /*-----Scene or Screen transition-----*/
    public void Jump2Title() { }
    public void SettingTopScreen() { }
    public void SettingBottomScreen() { }
    public void SettingNextScreen()
    {
        if (sc.CurrentIndex == 0)
        {
            // currently shown screen is top
            SettingBottomScreen();
        }
        else if (sc.CurrentIndex == 0)
        {
            // currently shown screen is Bottom
            SettingTopScreen();
        }
        //TODO: Call ChangeTopScreen or ChangeBottomScreen
        //      depend on current state
    }


    /*-----Setting Wolf-----*/
    public void InitWolf()
    {
        wolf_index = Random.Range(0, MAX_WOLF_SUSPECTS_NUM);
    }

    /*-----Load Script-----*/
    // TODO: Create Script Manager
    // TODO: Know your scripts!
    public void InitEvent()
    {
        EventPool = new List<EventData>();
        string path = Application.dataPath;

        foreach (string file in Directory.GetFiles(path + "/Xml\\", "*.xml"))
        {
            EventData eData = EventData.XmlLoad(file);
            EventPool.Add(eData);
        }
        Debug.Log(EventPool[0].Name);
        Debug.Log(EventPool[0].WolfText);

    }
    /*-----Game Setting-----*/
    public void Init()
    {
        //TODO: initialize game
        InitWolf();
        InitEvent();
    }

    public void Exit()
    {
        //TODO: Maybe Saving progress will needed
        Application.Quit();
    }

    private void Start()
    {
        Init();
    }

    public EventData getEventData()
    {
        if (Distopia < 5)
        {
            int t = 0;
            while (true)
            {
                t = Random.Range(0, 10);

                if (asdf[t] == 0)
                    break;
            }

            asdf[t] = 1;

            return EventPool[t];
        }
        else
            return EventPool[10];
    }

    void Update()
    {

    }

    void DistopiaIncrease()
    {
        Distopia++;

        foreach(AnimationImage AS in Man)
        {
            AS.SetAnchor(Distopia);
        }
    }

    public void SelectSuspect(int cin)
    {
        if(cin == wolf_index)
        {

        }
        else
        {
            DistopiaIncrease();
        }
    }
}
