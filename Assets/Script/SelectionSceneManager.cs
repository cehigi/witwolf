﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SelectionSceneManager : MonoBehaviour
{

    public GameObject[] buttons;
    public int size;
    int currentIndex;

    [SerializeField]
    GameObject selectedpanel;
    [SerializeField]
    SceneController scenecontroller;

    public int CurrentIndex
    {
        get
        {
            return currentIndex;
        }

        set
        {
            currentIndex = value;
            RequestTextUpdate();
            MoveSelected(currentIndex);
        }
    }

    void MoveSelected(int index)
    {
        if(index == -1)
        {
            selectedpanel.GetComponent<RectTransform>().position = new Vector3(-400, 0, 0);
            return;
        }
        selectedpanel.GetComponent<RectTransform>().position = buttons[index].GetComponent<RectTransform>().position;
    }

    void RequestTextUpdate()
    {
        GameManager d = GameManager.gameManager;
        if (d == null)
        {
            Debug.LogError("ashdfoiwheo");
        }
        int i = GameManager.gameManager.wolf_index;
        int c = CurrentIndex;

        var t = TextManager.textManager;
        if (c < i)
        {
            t.ShowNormalDialogue(c);
        }
        else if (c > i)
        {
            t.ShowNormalDialogue(c - 1);
        }
        else
        {
            t.ShowWolfDialogue();
        }
    }

    // Use this for initialization
    void Start()
    {
        //selectedpanel.GetComponent<RectTransform>().position = new Vector3(100, 0);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void setAnimation(bool flag)
    {
        //TODO: on/off animations
        for (int i = 0; i > size; i++)
        {
            buttons[i].GetComponent<Image>().enabled = flag;
        }
    }

    public void reloadScene()
    {
        currentIndex = -1;
        MoveSelected(-1);
        //TODO: reload bottom texts

    }

    public void SelectSuspect()
    {
        scenecontroller.SelectSuspect(currentIndex);
        Debug.Log(currentIndex);
        currentIndex = -1;
    }
}
