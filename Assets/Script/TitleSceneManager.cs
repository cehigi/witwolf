﻿using UnityEngine;
using System.Collections;

public class TitleSceneManager : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void OnGameStartButton()
    {
        // TODO: make game manager to set new game
        var g = GameManager.gameManager;
        g.Init();
    }

    public void OnExitButton()
    {
        var g = GameManager.gameManager;
        g.Exit();
        // TODO: make game manager to exit game
    }
}
