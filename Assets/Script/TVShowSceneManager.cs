﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TVShowSceneManager : MonoBehaviour {

    public GameObject leftImage;
    public GameObject middleImage;
    public GameObject rightText;

    public GameObject imagemage;
    public Sprite Hapy;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void setAnimation(bool flag)
    {
        //TODO: on/off animations
        middleImage.GetComponent<Image>().enabled = flag;
    }

    public void reloadScene(EventData data)
    {
        //TODO: change left image
        //leftObject
        //TODO: change middle image
        //TODO: change right text
        var t = TextManager.textManager;
        t.ReloadDialog(data);
    }

    public bool changeAnimation()
    {
        AnimationImage animate = middleImage.GetComponent<AnimationImage>();
        switch (animate.state)
        {
            case AnimationImage.State.idle:
                animate.state = AnimationImage.State.noize;
                break;
            case AnimationImage.State.noize:
                animate.state = AnimationImage.State.bignoize;
                break;
            case AnimationImage.State.bignoize:
                animate.state = AnimationImage.State.glitch;
                break;
            case AnimationImage.State.glitch:
                animate.state = AnimationImage.State.terrible;
                break;
            default:
                return true;
        }
        return false;
    }

    public void ENDEND()
    {
        imagemage.GetComponent<Image>().sprite = Hapy;
    }
}
