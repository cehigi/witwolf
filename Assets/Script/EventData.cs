﻿using System.Collections;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

public class EventData {
    public string Name;
    public string AnchorText;
    public string WolfText;
    public string NormalText_1;
    public string NormalText_2;
    public string NormalText_3;
    public string NormalText_4;

    static public EventData XmlLoad(string path)
    {
        EventData eData;
        using (TextReader reader = new StreamReader(path))
        {
            XmlSerializer ser = new XmlSerializer(typeof(EventData));
            eData = (EventData)ser.Deserialize(reader);
        }
        return eData;
    }
}
