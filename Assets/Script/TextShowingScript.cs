﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TextShowingScript : MonoBehaviour
{

    string targetString;
    int count = 0;
    int sizeString = 0;

    float clocktime;

    enum State
    {
        none,
        ready,
        play,
        done,
        EOF
    }

    State state;
    bool flag = true;

    // Use this for initialization
    void Start()
    {
        Reset();

        //setString("What The FUCK!");
        setString("");
        Show();
    }

    // Update is called once per frame
    void Update()
    {

        if ((int)((Time.time - clocktime) * 20) % 2 != 0)
        {
            flag = true;
            return;
        }
        if (flag)
            flag = false;
        else
            return;



        switch (state)
        {
            case State.play:
                if (sizeString <= this.gameObject.GetComponent<Text>().text.Length)
                    state = State.done;
                else
                {
                    this.gameObject.GetComponent<Text>().text = targetString.Substring(0, ++count);
                }
                break;
        }
    }

    public bool setString(string str)
    {
        Reset();
        targetString = str;
        state = State.ready;
        sizeString = targetString.Length;

        return true;
        //return false;
    }

    public void Reset()
    {
        this.gameObject.GetComponent<Text>().text = "";
        targetString = "";
        sizeString = 0;
        count = 0;

        clocktime = Time.deltaTime;

        state = State.none;
    }

    public void Refrest()
    {
        this.gameObject.GetComponent<Text>().text = "";
        count = 0;
    }

    public void Show()
    {
        state = State.play;
        clocktime = Time.time;
    }

    public void ShowFull()
    {
        state = State.play;
        count = sizeString-1;
    }
}
