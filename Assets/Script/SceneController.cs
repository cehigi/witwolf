﻿using UnityEngine;
using System.Collections;

public class SceneController : MonoBehaviour {
    public GameObject TopScene;
    public GameObject BottomScene;
    public GameObject TitleScene;
    public int moveSpeed;
    int currentIndex;
    bool is_ending;
    bool is_happy_ending;
    string happy_ending_text = "와아아아 찾아냈어";
    string bad_ending_text = "이세상은 내것이다";
    public bool Is_ending
    {
        get
        {
            return is_ending;
        }

        set
        {
            is_ending = value;
            User_selection = TopScene.GetComponent<SelectionSceneManager>().CurrentIndex;
            Debug.Log(User_selection);
        }
    }
    int user_selection;
    public int User_selection
    {
        get
        {
            return user_selection;
        }

        set
        {
            user_selection = value;
            GameManager d = GameManager.gameManager;
            if (d == null)
            {
                Debug.LogError("ashdfoiwheo");
            }
            int i = GameManager.gameManager.wolf_index;
            is_happy_ending = (i == user_selection);
        }
    }
    public int CurrentIndex
    {
        get { return currentIndex; }
    }
    bool moveScene = false;
    Rect currentRect;
    RectTransform currentRectTransform;
    Vector3 moveVelocity;
    Vector3 targetPosition;

    // Use this for initialization
    void Start () {
        currentRect = gameObject.GetComponent<RectTransform>().rect;
        currentRectTransform = gameObject.GetComponent<RectTransform>();
        currentIndex = 2;
        Debug.Log(TopScene.GetComponent<RectTransform>().rect);
        Debug.Log(TitleScene.GetComponent<RectTransform>().rect);
        Debug.Log(currentRectTransform.position);
    }
	
	// Update is called once per frame
	void Update () {
        
        if (moveScene)
        {
            currentRectTransform.position += moveVelocity;
            if((moveVelocity.y < 0 && currentRectTransform.position.y < targetPosition.y)
                || moveVelocity.y > 0 && currentRectTransform.position.y > targetPosition.y)
            {
                moveScene = false;
                currentRectTransform.position = targetPosition;
                if(currentIndex == 1)
                {
                    BottomScene.GetComponent<TVShowSceneManager>().setAnimation(true);
                    var t = TextManager.textManager;
                    if(Is_ending)
                    {
                        if (is_happy_ending) t.anchorText = happy_ending_text;
                        else t.anchorText = bad_ending_text;
                    }
                    t.ShowTVDialogue();
                    t.ClearSelectionDialogue();
                }
                else if(currentIndex == 2)
                {
                    TopScene.GetComponent<SelectionSceneManager>().setAnimation(false);
                    var t = TextManager.textManager;
                    t.ClearTVDialogue();
                }
            }
            //Debug.Log(currentRectTransform.position);
        }
    }

    public void changeScene(int to)
    {
        if(moveScene == false)
        {
            moveScene = true;
            moveVelocity = new Vector3(0, moveSpeed * (to - currentIndex) * 2, 0);
            targetPosition = currentRectTransform.position + new Vector3(0, TopScene.GetComponent<RectTransform>().rect.height, 0) * (to - currentIndex);
            if(currentIndex == 1)
            {
                BottomScene.GetComponent<TVShowSceneManager>().setAnimation(false);
            }
            else if(currentIndex == 0)
            {
                TopScene.GetComponent<SelectionSceneManager>().setAnimation(false);
            }
            
            if(to == 1)
            {
                if (currentIndex == 0)
                {
                    if (BottomScene.GetComponent<TVShowSceneManager>().changeAnimation()) is_ending = true;
                }
                BottomScene.GetComponent<TVShowSceneManager>().reloadScene(gameObject.GetComponent<GameManager>().getEventData());
            }
            else if(to == 0)
            {
                TopScene.GetComponent<SelectionSceneManager>().reloadScene();
            }
            currentIndex = to;
        }
    }

    public void SelectSuspect(int currentin)
    {
        changeScene(1);
        GameManager.gameManager.SelectSuspect(currentin);

        if (GameManager.gameManager.Distopia >= 5)
        {
            BottomScene.GetComponent<TVShowSceneManager>().ENDEND();
        }
    }

    void resetBottomScene()
    {


    }

    void resetTopScene()
    {

    }
}
